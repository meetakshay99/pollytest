#!/bin/sh

# Clone the aws repository.
echo "------- Now cloning AWS CPP SDK -------"
cd /demo/Sample
git clone https://github.com/aws/aws-sdk-cpp/

# Create a directory for compiling the aws sdk.
echo "------- Now creating AWS output directory -------"
mkdir /demo/Sample/aws-sdk-cpp_Linux
cd /demo/Sample/aws-sdk-cpp_Linux

# Build the aws sdk.
echo "------- Now building AWS CPP SDK -------"
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_ONLY=polly -DTARGET_ARCH=LINUX /demo/Sample/aws-sdk-cpp
make

# Copy the created libs to the binary folder.
echo "------- Now copying the generated AWS Polly libs to the unreal app -------"
mkdir -p /demo/Sample/Docker/pollytest/Polly/Binaries/Linux
cp /demo/Sample/aws-sdk-cpp_Linux/aws-cpp-sdk-core/libaws-cpp-sdk-core.so /demo/Sample/Docker/pollytest/Polly/Binaries/Linux
cp /demo/Sample/aws-sdk-cpp_Linux/aws-cpp-sdk-polly/libaws-cpp-sdk-polly.so /demo/Sample/Docker/pollytest/Polly/Binaries/Linux

# Build the unreal app.
echo "------- Now building the unreal app -------"
cd /demo/Sample/Docker/pollytest/Polly
ue4 build
ue4 run -game -AudioMixer -windowed -ResX=640 -ResY=360
