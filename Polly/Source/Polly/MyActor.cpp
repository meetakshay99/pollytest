// Fill out your copyright notice in the Description page of Project Settings.

#include "MyActor.h"

using namespace Aws::Polly;

// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AMyActor::createClientConfig() {
    if (config == NULL) {
        config = new ClientConfiguration();
        config->region = "eu-west-1";
    }
}

void AMyActor::createCredentials() {
    if (credentials == NULL) {
        credentials = new AWSCredentials();
        credentials->SetAWSSecretKey("oL65+hsHyjDUh7l9erGqBC9muq2");
        credentials->SetAWSAccessKeyId("AKIAIBRYTQ");
    }
}

void AMyActor::createPollyClient() {
    if (pollyClient == NULL) {
        pollyClient = new PollyClient(*credentials, *config);
    }
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
    UE_LOG(LogTemp, Log, TEXT("Before creating PollyClient"));
    Aws::SDKOptions options;
    Aws::InitAPI(options);
    createCredentials();
    createClientConfig();
    createPollyClient();
    UE_LOG(LogTemp, Log, TEXT("After creating PollyClient"));
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    
}

