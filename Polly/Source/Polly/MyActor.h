// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <aws/polly/PollyClient.h>
#include <aws/core/Aws.h>
#include <aws/core/client/ClientConfiguration.h>
#include <aws/core/auth/AWSCredentialsProvider.h>
#include "MyActor.generated.h"

using namespace Aws::Polly;
using namespace Aws::Client;
using namespace Aws::Auth;

UCLASS()
class POLLY_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    PollyClient *pollyClient = NULL;
    ClientConfiguration *config = NULL;
    AWSCredentials *credentials = NULL;

    void createClientConfig();
    void createCredentials();
    
    
public:
    void createPollyClient();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
