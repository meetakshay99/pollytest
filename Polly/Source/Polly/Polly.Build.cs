// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.IO;
using System;

public class Polly : ModuleRules
{
    private string ThirdPartyPath {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/")); }
    }

    public bool LoadPolly(ReadOnlyTargetRules Target) {
        Log.TraceInformation("LoadPolly 1");
        
        // Start Polly linking here!
        bool isLibrarySupported = false;
        Log.TraceInformation("LoadPolly 2");
        
        // Create Polly Path.
        string PollyPath = Path.Combine(ThirdPartyPath, "Polly");
        Log.TraceInformation("PollyPath = " + PollyPath);
        Log.TraceInformation("LoadPolly 3");
        
        // Get Library Path
        String LibPath = "";
        String HeaderPath = "";
        if (Target.Platform == UnrealTargetPlatform.Mac) {
            LibPath = Path.Combine(PollyPath, "Libraries", "Mac");
            HeaderPath = Path.Combine(PollyPath, "Includes");
            isLibrarySupported = true;
            Log.TraceInformation("LoadPolly 4 Mac");
            
        } else if (Target.Platform == UnrealTargetPlatform.Linux) {
            LibPath = Path.Combine(PollyPath, "Libraries", "Linux");
            HeaderPath = Path.Combine(PollyPath, "Includes");
            isLibrarySupported = true;
            Log.TraceInformation("LoadPolly 4 Linux");
        } else {
            string Err = string.Format("{0} dedicated server is made to depend on {1}. We want to avoid this, please correct module dependencies.", Target.Platform.ToString(), this.ToString()); System.Console.WriteLine(Err);
            Log.TraceInformation("LoadPolly 5");
        }
        
        Log.TraceInformation("LoadPolly 6");
        if (isLibrarySupported) {
            Log.TraceInformation("LibPath = " + LibPath);
            Log.TraceInformation("HeaderPath = " + HeaderPath);
            
            PublicLibraryPaths.AddRange(new string[] { LibPath });
            PublicIncludePaths.AddRange(new string[] { HeaderPath });
            
            DirectoryInfo libPathDirInfo = new DirectoryInfo(LibPath);
            FileInfo[] files = libPathDirInfo.GetFiles();
            foreach (FileInfo file in files) {
                string tempPath = Path.Combine(LibPath, file.Name);
                PublicAdditionalLibraries.Add(tempPath);
            }
        }
        Log.TraceInformation("LoadPolly 7");
        return isLibrarySupported;
    }

	public Polly(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        if (Target.Platform == UnrealTargetPlatform.Mac || Target.Platform == UnrealTargetPlatform.Linux) {
            LoadPolly(Target);
        }
		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
